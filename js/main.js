
$(".btn_modal").fancybox({
    'padding'    : 0
});

(function() {

    $('.dropnav__nav > li > a').mouseenter(function(){

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.dropnav');

        $(this).closest('.dropnav__nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('.dropnav__item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());


$('.one__slider').slick({
    dots: true,
    arrows: true,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slider_nav prev"></span>',
    nextArrow: '<span class="slider_nav next"></span>'
});


var services = $('.services__slider').slick({
    dots: false,
    arrows: true,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slider_nav prev"></span>',
    nextArrow: '<span class="slider_nav next"></span>'
});


services.on('beforeChange', function(event, slick, currentSlide, nextSlide){
    var slide = nextSlide + 1;
    var elem = '.cat_elem_0' + slide;
    var cat  = 'active_0' + slide;

    $('.services').find('.services__cat_elem').removeClass('active');
    $('.services').find(elem).addClass('active');

    $(".services__cat").removeClassWild("active_*");
    $(".services__cat").addClass(cat);
});

$('.services__cat_elem span').on('click touchstart', function(e){
    e.preventDefault();
    var elem = $(this).attr("data-slide");
    services.slick('slickGoTo', elem, true);
});



$('.comments__slider').slick({
    dots: false,
    arrows: false,
    autoplay: true,
    slidesToShow: 2,
    slidesToScroll: 1
});

$('.comments_nav.prev').on('click touchstart', function(e){
    e.preventDefault();
    $('.comments__slider').slick('slickPrev');
});

$('.comments_nav.next').on('click touchstart', function(e){
    e.preventDefault();
    $('.comments__slider').slick('slickNext');
});



$('.instructor__slider').slick({
    dots: false,
    arrows: false,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1
});

$('.instructor_nav.prev').on('click touchstart', function(e){
    e.preventDefault();
    $('.instructor__slider').slick('slickPrev');
});

$('.instructor_nav.next').on('click touchstart', function(e){
    e.preventDefault();
    $('.instructor__slider').slick('slickNext');
});



$('.advantages__slider').slick({
    dots: true,
    arrows: false,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1
});

$('.advantages_nav.prev').on('click touchstart', function(e){
    e.preventDefault();
    $('.advantages__slider').slick('slickPrev');
});

$('.advantages_nav.next').on('click touchstart', function(e){
    e.preventDefault();
    $('.advantages__slider').slick('slickNext');
});


(function($) {
    $.fn.removeClassWild = function(mask) {
        return this.removeClass(function(index, cls) {
            var re = mask.replace(/\*/g, '\\S+');
            return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
        });
    };
})(jQuery);